using System.Collections;
using System.Collections.Generic;
using JetBrains.Annotations;
using UnityEngine;

public class Player : MonoBehaviour
{
    /* Test mov
    [SerializeField] float speed;
    [SerializeField] float rotationSpeed;
    [SerializeField] float sens;
    
    float rotationX;
    float rotationY;
    */

    [SerializeField] Vector2 turn;
    [SerializeField] float sens;
    [SerializeField] Vector3 deltaMove;
    [SerializeField] float speed;
    [SerializeField] GameObject mov;
    
    void Start() 
    {
        Cursor.lockState = CursorLockMode.Locked;    
    }
   
    void Update()
    {
        
        //Movement();   
        //CameraMov();
        MovNoClip();
    }

/*Mov player
    void Movement()
    {
        float horizontal = Input.GetAxis("Horizontal");
        float vertical = Input.GetAxis("Vertical");

        Vector3 movDirection = new Vector3(horizontal, 0, vertical);
        movDirection.Normalize();

        transform.position = transform.position + movDirection * speed * Time.deltaTime;
        if(movDirection != Vector3.zero) transform.rotation = Quaternion.Slerp(transform.rotation,Quaternion.LookRotation(movDirection),rotationSpeed * Time.deltaTime);
    }
    */

/*Camera
    void CameraMov()
    {
        rotationY += Input.GetAxis("Mouse X") * sens;
        rotationX += Input.GetAxis("Mouse Y") * -1 * sens;

        transform.localEulerAngles = new Vector3(rotationX, rotationY,0);
 
    }
*/

    void MovNoClip()
    {
        turn.x += Input.GetAxis("Mouse X") * sens;
        turn.y += Input.GetAxis("Mouse Y") * sens;
        mov.transform.localRotation = Quaternion.Euler(0,turn.x,0);
        transform.localRotation = Quaternion.Euler(-turn.y,0,0);

        deltaMove = new Vector3(Input.GetAxisRaw("Horizontal"), 0, Input.GetAxisRaw("Vertical")) * speed * Time.deltaTime;
        //transform.position = transform.position + deltaMove * speed * Time.deltaTime;

        if(Input.GetKey(KeyCode.Space))
        {
            transform.position = Vector3.up * speed * Time.deltaTime;
        }

        
        if(Input.GetKey(KeyCode.LeftControl))
        {
            transform.position = Vector3.down * speed * Time.deltaTime;
        }
    }

}
