using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mov02 : MonoBehaviour
{
    
    [SerializeField] float speed;
     
    void Update()
    {
        transform.Rotate(0.1f,0.3f,0.25f * Time.deltaTime * speed);
    }
}
