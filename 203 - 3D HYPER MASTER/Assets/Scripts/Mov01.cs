using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class Mov01 : MonoBehaviour
{ 
    [SerializeField] float speed;
   [SerializeField] Transform[] _position;
   [SerializeField] float objSpeed;
   int nextPosIndex;
   Transform nextPos;

    void Start() 
    {
        nextPos = _position[0];    
    }

    void Update()
    {
        MovLogic();
        transform.Rotate(0, 0.3f, 0.05f * Time.deltaTime * speed);
    }

    void MovLogic()
    {
        if(transform.position == nextPos.position)
        {
            nextPosIndex++;
            if(nextPosIndex>=_position.Length)
            {
                nextPosIndex = 0;
            }
            nextPos = _position[nextPosIndex];
        }
        else 
        {
            transform.position = Vector3.MoveTowards(transform.position, nextPos.position, objSpeed * Time.deltaTime);
        }
         

    }
}
