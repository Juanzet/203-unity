using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public static class SaveLoadSVGData 
{
    //GUARDO EL ARCHIVO
    public static void SaveData<T>(T data, string path, string fileName)
    {
        string fullPath = Application.persistentDataPath + "/" + path + "/";
        bool checkFolderExist = Directory.Exists(fullPath);

        if(checkFolderExist == false) 
        {
            Directory.CreateDirectory(fullPath);
        }

        //transformo el archivo a json
        string json = JsonUtility.ToJson(data);
        // lo transformo a texto pongo el nombre y la direccion y el objeto es el mismo archivo que transformo a json (CREO EL ARCHIVO EN EL DESPOSITIVO)
        File.WriteAllText(fullPath + fileName + ".json", json);
        Debug.Log("Guardado correctamente en " + fullPath);
    }

    //CARGO EL ARCHIVO
    public static T LoadData<T>(string path, string fileName)
    {
        string fullPath = Application.persistentDataPath + "/" + path + "/" + fileName + ".json";

        //preguntamos si el archivo existe
        if(File.Exists(fullPath))
        {
            string jsonReader = File.ReadAllText(fullPath);
            var obj = JsonUtility.FromJson<T>(jsonReader);
            return obj;
        }
        else 
        {
            Debug.Log("No se encontro el archivo");
            //al no saber que tipo de informacion retornar no se pone NULL sino Default
            return default;
        }
    }
}
