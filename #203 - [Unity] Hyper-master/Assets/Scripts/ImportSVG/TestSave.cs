using System.Collections;
using System.Collections.Generic;
using TMPro;
using Unity.VectorGraphics;
using UnityEngine;
using UnityEngine.UI;

public class TestSave : MonoBehaviour
{
   public SVGImage _svg;
   public SVGImage _svg1;
   public TMP_Text txt;

   public ExampleData data;
   public const string PATH_DATA = "Data/test";
   public const string NAME_FILE_DATA = "ExampleData";

    void Start() 
    {
        var dataFound = SaveLoadSVGData.LoadData<ExampleData>(PATH_DATA,NAME_FILE_DATA);
        if(dataFound != null)
        {
            data = dataFound;
        }
        else 
        {
            data = new ExampleData();
            SaveData();
            
        }
    }

    public void TestSVG()
    {
        data.svg00.sprite = _svg.sprite;
        data.svg01.sprite = _svg1.sprite;
        txt.text = "Se cambio la imagen con exito";
        Debug.Log("Se Cambio la imagen");
        SaveData();
    }

    void SaveData()
    {
        SaveLoadSVGData.SaveData(data, PATH_DATA, NAME_FILE_DATA);
    }
   
}
