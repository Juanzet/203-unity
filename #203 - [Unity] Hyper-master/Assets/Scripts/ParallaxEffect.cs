using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParallaxEffect : MonoBehaviour
{
    [SerializeField] Vector2 velParallax;
    Vector2 offset;
    Material mat;

    void Awake()
    {
        mat = GetComponent<SpriteRenderer>().material;
    }

    
    void Update()
    {
        offset = velParallax * Time.deltaTime;
        mat.mainTextureOffset += offset;
    }
}
