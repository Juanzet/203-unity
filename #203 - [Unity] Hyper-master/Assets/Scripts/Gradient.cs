using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Gradient : MonoBehaviour
{

    #region variables de gradient
    public Image img;
    public Color[] colors;
    int currentColorI = 0;
    int targetColorI = 1;
    float targetPoint;
    public float time;
    #endregion
    
    void Update()
    {
        ColorLogic();
    }

    void ColorLogic() 
    {
        targetPoint += Time.deltaTime;
        img.color = Color.Lerp(colors[currentColorI], colors[targetColorI], targetPoint);
        if(targetPoint >= 1f)
        {
            targetPoint = 0f;
            currentColorI = targetColorI;
            targetColorI++;

            if(targetColorI == colors.Length)
                targetColorI = 0;
        }
    }
}
