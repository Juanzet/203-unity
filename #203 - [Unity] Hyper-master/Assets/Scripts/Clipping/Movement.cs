using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour
{
    float horizontal;
    float vertical;
    [SerializeField] float moveSpeed;
    Vector2 moveDirection;
    [SerializeField] Rigidbody2D rb;

    void Update() 
    {
        Inputs();
    }

    void Inputs()
    {
        horizontal = Input.GetAxisRaw("Horizontal");
        vertical = Input.GetAxisRaw("Vertical");        

        moveDirection = new Vector2(horizontal,vertical).normalized;
        //esto tendria que ir en el FixedUpdate porque trabaja con fisicas peero como es para mostrar el clipping no importa
        rb.velocity = new Vector2(moveDirection.x * moveSpeed, moveDirection.y * moveSpeed);

    }
}
