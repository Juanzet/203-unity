using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.UIElements;

public class UIManager : MonoBehaviour
{
    //[SerializeField] GameObject parallax;
    [SerializeField] GameObject pllx1;
    [SerializeField] GameObject pllx2;
    [SerializeField] GameObject graddientEffect;
    [SerializeField] GameObject svg;
    //[SerializeField] GameObject back;
    
    void Start() 
    {
       pllx1 = GameObject.Find("Background");
       pllx2 = GameObject.Find("Background2");
       graddientEffect = GameObject.Find("GradientEffect");  
       svg = GameObject.Find("SVG");  
    }
    void Update()
    {
        if(Input.GetKeyDown(KeyCode.Escape))
        {
            SceneManager.LoadScene("ParallaxEffectBG");
        }
    }

    public void Clipping()
    {
        SceneManager.LoadScene("Clipping");
    }
    public void ParallaxV1()
    {
        pllx1.SetActive(true);
        pllx2.SetActive(false);
    }    
    public void ParallaxV2()
    {
        
        pllx1.SetActive(false);
        pllx2.SetActive(true);
    }    
    public void SVG()
    {
        //parallax.SetActive(false);
        graddientEffect.SetActive(false);
        svg.SetActive(true);
    }
    public void Graddient()
    {
        graddientEffect.SetActive(true);
        //parallax.SetActive(false);
        svg.SetActive(false);
    }
}
